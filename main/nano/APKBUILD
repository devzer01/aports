# Contributor: Michael Mason <ms13sp@gmail.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=nano
pkgver=5.0
pkgrel=0
pkgdesc="Enhanced clone of the Pico text editor"
url="https://www.nano-editor.org"
arch="all"
license="GPL-3.0-or-later"
makedepends="file-dev linux-headers ncurses-dev"
subpackages="$pkgname-doc $pkgname-syntax::noarch"
source="https://www.nano-editor.org/dist/v${pkgver%.*}/nano-$pkgver.tar.xz"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--disable-nls
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	install -Dm644 doc/sample.nanorc \
		"$pkgdir"/etc/nanorc

	# Proper syntax highlighting for APKBUILDs
	sed -i "/syntax/s/\"$/\" \"APKBUILD&/" \
		"$pkgdir"/usr/share/nano/sh.nanorc

	rm -rf "$pkgdir"/usr/lib/charset.alias
}

syntax() {
	pkgdesc="Syntax highlighting definitions for $pkgname"

	mkdir -p "$subpkgdir"/usr/share/$pkgname/
	mv "$pkgdir"/usr/share/$pkgname/*.nanorc \
		"$subpkgdir"/usr/share/$pkgname/
}

sha512sums="35e8028be93e8770c323a55111690138e5ad0bcf224b2e960488ce78d9da00ea631a85ed756a67a1c62d7a138f4f2f6b674eadc8eaa37091f32743a3b669fbd2  nano-5.0.tar.xz"
